---

- name: Create git group
  group:
    name: git
    state: present

- name: Setup SSH to accept GIT_PROTOCOL env
  copy:
    content: |
      AcceptEnv GIT_PROTOCOL
    dest: /etc/ssh/sshd_config.d/forgejo.conf
    owner: root
    group: root
    mode: 0644
  notify: reload sshd

- name: Create git-static group
  group:
    name: git-static
    state: present

- name: Forgejo user
  user:
    system: true
    password: '!'
    home: /home/git
    shell: /bin/bash
    comment: "Git Version Control"
    group: git
    name: git

- name: Forgejo static user  # To compile and own static content
  user:
    system: true
    password: '!'
    comment: "To compile and own static forgejo content."
    group: git-static
    name: git-static

- name: Download forgejo
  get_url:
    dest: /usr/local/bin/forgejo
    url: "https://codeberg.org/forgejo/forgejo/releases/download/v{{ forgejo_version }}/forgejo-{{ forgejo_version }}-linux-amd64"
    mode: 0755
    owner: root
    group: root
  notify: restart forgejo

- name: Install dependencies
  package:
    name:
      - anacron
      - git
      - postgresql
      - curl
      - python3-psycopg2  # For Ansible
      # - rsync  # for static file generation
      # - nodejs # for static file generation
      # - npm    # for static file generation
      # - make   # for static file generation
      - nginx
      - pandoc # For reStructuredText rendering
    state: present

- name: Ensure locale en_US.UTF-8 exists
  locale_gen:
    name: en_US.UTF-8
    state: present
  register: locale_gen_result

- name: Force-restart PostgreSQL after new locales are generated
  service:
    name: postgresql
    state: restarted
  when: locale_gen_result.changed

- name: Create psql git user
  become: true
  become_user: postgres
  postgresql_user:
    user: git

- name: Create psql forgejo DB
  become: true
  become_user: postgres
  postgresql_db:
    name: forgejo
    owner: git
    encoding: UTF-8
    lc_collate: en_US.UTF-8
    lc_ctype: en_US.UTF-8
    template: template0

- name: Collect PostgreSQL version and extensions
  become: yes
  become_user: postgres
  postgresql_info:
    filter: ver*
  register: db_info

- name: Configure psql
  notify: reload psql
  copy:
    dest: "/etc/postgresql/{{ db_info.version.major }}/main/conf.d/afpy.conf"
    owner: postgres
    group: postgres
    mode: 0644
    content: |
      log_min_duration_statement = 1000
      log_checkpoints = on
      log_connections = on
      log_disconnections = on
      log_lock_waits = on
      log_temp_files = 0
      log_autovacuum_min_duration = 0
      log_error_verbosity = default
      work_mem = 128MB
      shared_buffers = 1GB
      lc_messages='C'
      timezone='UTC'

- name: Create forgejo hierarchy
  file:
    state: directory
    mode: 0750
    owner: git
    group: git
    path: "{{ item }}"
  loop:
    - /var/lib/forgejo/data
    - /var/lib/forgejo/log
    - /var/lib/forgejo/custom/templates/custom/
    - /var/lib/forgejo/custom/public/img/

- name: Create forgejo config hierarchy
  file:
    state: directory
    mode: 0750
    owner: root
    group: git
    path: /etc/forgejo

- name: Setup nginx
  include_role: name=nginx
  vars:
    nginx_domain: git.afpy.org
    nginx_certificates: ['git.afpy.org']
    nginx_conf: |
      limit_req_zone $binary_remote_addr zone=ip:10m rate=4r/s;

      server
      {
          listen [::]:80; listen 80;
          server_name git.afpy.org;
          return 301 https://git.afpy.org$request_uri;
      }

      server
      {
          listen [::]:443 ssl http2; listen 443 ssl http2;
          server_name git.afpy.org;
          include snippets/letsencrypt-git.afpy.org.conf;
          client_max_body_size 16M;

          add_header Content-Security-Policy-Report-Only "default-src 'self'; connect-src 'self'; font-src 'self' data:; form-action 'self'; img-src 'self' https: data:; manifest-src 'self' data:; object-src 'self'; script-src 'self' 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; worker-src 'self'";
          # See add_header Content-Security-Policy-Report-Only "
          add_header X-Content-Type-Options "nosniff";

          # location /_/static/assets/ {
          #     alias /var/lib/forgejo-static/public/;
          # }

          limit_req zone=ip burst=10;

          location = /robots.txt {
              default_type "text/plain";
              return 200 "User-agent: *
      Disallow: /api/*
      Disallow: /avatars
      Disallow: /user/*
      Disallow: /*/*/src/commit/*
      Disallow: /*/*/commit/*
      Disallow: /*/*/*/refs/*
      Disallow: /*/*/*/star
      Disallow: /*/*/*/watch
      Disallow: /*/*/labels
      Disallow: /*/*/activity/*
      Disallow: /vendor/*
      Disallow: /swagger.*.json

      Disallow: /explore/*?*

      Disallow: /repo/create
      Disallow: /repo/migrate
      Disallow: /org/create
      Disallow: /*/*/fork

      Disallow: /*/*/watchers
      Disallow: /*/*/stargazers
      Disallow: /*/*/forks

      Disallow: /*/*/activity
      Disallow: /*/*/projects
      Disallow: /*/*/commits/
      Disallow: /*/*/branches
      Disallow: /*/*/tags
      Disallow: /*/*/compare
      Disallow: /*/*/lastcommit/*

      Disallow: /*/*/issues/new
      Disallow: /*/*/issues/?*
      Disallow: /*/*/issues?*
      Disallow: /*/*/pulls/?*
      Disallow: /*/*/pulls?*
      Disallow: /*/*/pulls/*/files

      Disallow: /*/tree/
      Disallow: /*/download
      Disallow: /*/revisions
      Disallow: /*/commits/*?author
      Disallow: /*/commits/*?path
      Disallow: /*/comments
      Disallow: /*/blame/
      Disallow: /*/raw/
      Disallow: /*/cache/
      Disallow: /.git/
      Disallow: */.git/
      Disallow: /*.git
      Disallow: /*.atom
      Disallow: /*.rss

      Disallow: /*/*/archive/
      Disallow: *.bundle
      Disallow: */commit/*.patch
      Disallow: */commit/*.diff

      Disallow: /*lang=*
      Disallow: /*source=*
      Disallow: /*ref_cta=*
      Disallow: /*plan=*
      Disallow: /*return_to=*
      Disallow: /*ref_loc=*
      Disallow: /*setup_organization=*
      Disallow: /*source_repo=*
      Disallow: /*ref_page=*
      Disallow: /*source=*
      Disallow: /*referrer=*
      Disallow: /*report=*
      Disallow: /*author=*
      Disallow: /*since=*
      Disallow: /*until=*
      Disallow: /*commits?author=*
      Disallow: /*tab=*
      Disallow: /*q=*
      Disallow: /*repo-search-archived=*

      Crawl-delay: 2

      User-agent: Amazonbot
      User-agent: anthropic-ai
      User-agent: Applebot-Extended
      User-agent: Bytespider
      User-agent: CCBot
      User-agent: ChatGPT-User
      User-agent: ClaudeBot
      User-agent: Claude-Web
      User-agent: cohere-ai
      User-agent: Diffbot
      User-agent: FacebookBot
      User-agent: facebookexternalhit
      User-agent: FriendlyCrawler
      User-agent: Google-Extended
      User-agent: GPTBot
      User-agent: ICC-Crawler
      User-agent: ImagesiftBot
      User-agent: img2dataset
      User-agent: meta-externalagent
      User-agent: OAI-SearchBot
      User-agent: Omgili
      User-agent: Omgilibot
      User-agent: PerplexityBot
      User-agent: PetalBot
      User-agent: Scrapy
      User-agent: Timpibot
      User-agent: VelenPublicWebCrawler
      User-agent: YouBot
      Disallow: /";
          }

          location / {
              proxy_pass http://localhost:3001;
              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header X-Forwarded-Proto $scheme;
          }

      }


# Public asset generation (to allow nginx to serve them) needs nodejs>14.

# - name: Create forgejo static hierarchy
#   file:
#     state: directory
#     mode: 0755
#     owner: git-static
#     group: git-static
#     path: "{{ item }}"
#   loop:
#     - /var/lib/forgejo-static/source
#     - /var/lib/forgejo-static/public
#
# - name: Download forgejo tarball  # For the static content
#   unarchive:
#     src: "https://github.com/go-forgejo/forgejo/archive/refs/tags/v{{ forgejo_version }}.tar.gz"
#     dest: /var/lib/forgejo-static/source/
#     remote_src: true
#     owner: git-static
#     group: git-static
#   register: download_forgejo_tarball
#
# - name: Compile static assets
#   command: make frontend
#   args:
#     chdir: "/var/lib/forgejo-static/source/forgejo-{{ forgejo_version }}"
#   become: true
#   become_user: git-static
#   when: download_forgejo_tarball is changed
#
# - name: Copy public assets
#   synchronize:
#     src: "/var/lib/forgejo-static/source/forgejo-{{ forgejo_version }}/public/"
#     dest: "/var/lib/forgejo-static/public/"
